import generators as g

from datetime import datetime, timedelta

from identifier_pb2 import Identifier
from invoice import InvoiceProcessor
from invoice_pb2 import Invoice, NO_CREATED_DATE, NO_ITEMS, \
    FUTURE_DATE

def identifierStub():
  class Stub:
    def Generate(empty, context):
      return Identifier(code='TRZ987')
  return Stub()

def test_identifier_is_assigned():
  processor = InvoiceProcessor(identifierStub())
  invoice = g.invoice()
  result = processor.Process(invoice, None)
  assert result.id == 'TRZ987'

def test_processed_time_is_set():
  processor = InvoiceProcessor(identifierStub())
  invoice = g.invoice()
  assert not invoice.HasField('processed')
  result = processor.Process(invoice, None)
  assert result.HasField('processed')

def test_when_invoice_is_invalid_identifier_is_not_assigned():
  processor = InvoiceProcessor(identifierStub())
  invoice = Invoice()
  result = processor.Process(invoice, None)
  assert len(result.errors) > 0
  assert result.id == ''

def test_invoice_is_invalid_when_created_is_empty():
  processor = InvoiceProcessor(identifierStub())
  invoice = g.invoice()
  invoice.ClearField("created")
  assert not invoice.HasField("created")
  result = processor.Process(invoice, None)
  assert len(result.errors) == 1
  assert result.errors[0] == NO_CREATED_DATE

def test_invoice_is_invalid_when_there_are_no_items():
  processor = InvoiceProcessor(identifierStub())
  invoice = g.invoice()
  invoice.ClearField("items")
  assert len(invoice.items) == 0
  result = processor.Process(invoice, None)
  assert len(result.errors) == 1
  assert result.errors[0] == NO_ITEMS

def test_invoice_date_cannot_be_in_the_future():
  processor = InvoiceProcessor(identifierStub())
  invoice = g.invoice()
  invoice.created.FromDatetime(datetime.now() + timedelta(days=20))
  result = processor.Process(invoice, None)
  assert len(result.errors) == 1
  assert result.errors[0] == FUTURE_DATE

