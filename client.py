import grpc

from google.protobuf import empty_pb2

from invoice_pb2_grpc import InvoiceProcessorStub
from identifier_pb2_grpc import IdentifierGeneratorStub

import generators as g

def invoice():
  channel = grpc.insecure_channel('localhost:50051')
  stub = InvoiceProcessorStub(channel)
  response = stub.Process(g.invoice())
  print(response)

def identifier():
  channel = grpc.insecure_channel('localhost:50052')
  stub = IdentifierGeneratorStub(channel)
  identifier = stub.Generate(empty_pb2.Empty())
  print("Identifier")
  print(identifier)

if __name__ == '__main__':
  invoice()
  identifier()

